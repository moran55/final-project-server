﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalProject.Common.Models
{
    public class User
    {
        public User() { }
        public Guid UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime? BirthDate { get; set; }
        public bool? IsHasDrivingLicense { get; set; }
        public TimeSpan? AvailableStartTask { get; set; }
        public TimeSpan? AvailableEndTask { get; set; }
        public bool? IsParent { get; set; }
        public Int64? Score { get; set; }
        public int Age { get; set; }
        public Guid? FamilyId { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public List<taskForUser> TasksList { get; set; }
    }

    public class taskForUser
    {
        public String TaskName { get; set; }
        public Guid TaskId { get; set; }
        public Guid UserId { get; set; }
        public int IsLikeTask { get; set; }
    }


}

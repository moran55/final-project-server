﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalProject.Common.Models
{
    public class TaskHours
    {
        public Guid TaskHoursId { get; set; }
        public DateTime? startTask { get; set; }
        public DateTime? EndTask { get; set; }
        public string DaysList { get; set; }
        public bool isComplete { get; set; }
        public Guid UserFromJoiner { get; set; }
        public Guid TaskId { get; set; }
    }
}

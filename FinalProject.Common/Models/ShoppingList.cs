﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalProject.Common.Models
{
    public class ShoppingList
    {
        public Guid ShoppingListId{ get; set; }
        public string ShoppingListName { get; set; }
        public bool? IsComplete { get; set; }
        public Guid? TaskId { get; set; }
        public Guid? FamilyId { get; set; }
        public List<ShoppingItem> shoppingItems { get; set; }
    }
}

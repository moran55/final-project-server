﻿using FinalProject.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalProject.Common
{
    public class Task
    {
        public Task(){}
        public Guid TaskId { get; set; }
        public string TaskName { get; set; }
        public string Description { get; set; }
        public bool? IsDrivingLicenseNeeded { get; set; }
        public bool? IsParentNeeded { get; set; }
        public bool? IsRecursive { get; set; }
        public bool? IsComplete { get; set; }
        public string Days { get; set; }
        public Int64? TaskScore { get; set; }
        public string UserFullName { get; set; }
        public Guid?  DoingTaskUserId { get; set; }
        public Guid? UserId { get; set; }
        public List<TaskHours> TaskHours { get; set; }
        public bool IsShoppingListTask { get; set; }
        public string ShoppingListName { get; set; }
    }
}

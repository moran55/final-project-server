﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalProject.Common.Models
{
    public class Requests
    {
        #region ShoppingItem
        public class CompleteChange
        {
            public List<String> ShoppingItemsTrueIds { get; set; }
            public List<String> ShoppingItemsFalseIds { get; set; }
        }

        public class ShoppingItemToDelete
        {
            public String ShoppingItemId { get; set; }
        }

        public class AddNewShoppingItem
        {
            public String ShoppingListId { get; set; }
            public bool IsComplete { get; set; }
            public String ItemName { get; set; }
            public String Amount { get; set; }
        }
        #endregion

        #region ShoppingList
        public class NewShoppingList
        {
            public string ShoppingListName { get; set; }
            public List<ShippingItem> ShoppingItems { get; set; }
        }

        public class ShippingItem
        {
            public string ItemName { get; set; }
            public int Quantity { get; set; }
        }

        public class ShoppingToTask
        {
            public Guid TaskId { get; set; }
            public Guid ShoppingListId { get; set; }
        }

        #endregion

        #region Task
        public class UpdateTask
        {
            public String TaskId { get; set; }
            public String UserId { get; set; }
            public String TaskName { get; set; }
            public String Description { get; set; }
            public bool IsParentNeeded { get; set; }
            public bool IsDrivingLicenseNeeded { get; set; }
            public String startTaskDate { get; set; }
            public String startTaskTime { get; set; }
            public String endTaskDate { get; set; }
            public String endTaskTime { get; set; }
            public string[] Days { get; set; }
            public List<IsCompleteTaskHours> isCompleteList { get; set; }
        }

        public class NewTask
        {
            public String UserId { get; set; }
            public String TaskName { get; set; }
            public String Description { get; set; }
            public bool IsParentNeeded { get; set; }
            public bool IsDrivingLicenseNeeded { get; set; }
            public bool IsComplete { get; set; }
            public bool IsRecursive { get; set; }
            public Days Days { get; set; }
            public Int64 TaskScore { get; set; }
            public String startTaskDate { get; set; }
            public String startTaskTime { get; set; }
            public String endTaskDate { get; set; }
            public String endTaskTime { get; set; }
        }

        public class Days
        {
            public bool sunday { get; set; }
            public bool monday { get; set; }
            public bool tuesday { get; set; }
            public bool wednesday { get; set; }
            public bool thursday { get; set; }
            public bool friday { get; set; }
            public bool saturday { get; set; }

        }

        public class IsCompleteTaskHours
        {
            public String TaskHoursId { get; set; }
            public bool IsComplete { get; set; }
        }

        public class RemoveTask
        {
            public String TaskId { get; set; }
        }

        public class AlgorithmTTaskByUser
        {
            public String TaskId { get; set; }
        }

        public class TasksListForIsLike
        {
            public String TaskId { get; set; }
            public int IsLike { get; set; }
        }
        #endregion

        #region User

        public class NewUser
        {
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public bool IsParent { get; set; }
            public bool IsHasDrivingLiecence { get; set; }
            public string BirthDate { get; set; }
            public string AvailableStartTask { get; set; }
            public string AvailableEndTask { get; set; }
            public string UserName { get; set; }
            public string Password { get; set; }
        }

        #endregion

        #region Family

        public class NewMember
        {
            public string familyId { get; set; }
            public string email { get; set; }
        }

        #endregion
    }
}

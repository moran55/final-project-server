﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalProject.Common.Models
{
    public class ShoppingItem
    {
        public Guid ShoppingItemId{ get; set; }
        public string ItemName { get; set; }
        public float Amount { get; set; }
        public float TotalPrice { get; set; }
        public bool IsComplete { get; set; }
        public string Comment { get; set; }
        public Guid ShoppingListId { get; set; }
    }
}

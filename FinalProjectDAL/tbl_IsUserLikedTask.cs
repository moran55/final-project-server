//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace FinalProjectDAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_IsUserLikedTask
    {
        public System.Guid IsLikeTaskId { get; set; }
        public Nullable<System.Guid> TaskId { get; set; }
        public Nullable<System.Guid> UserId { get; set; }
        public Nullable<bool> IsLikeTask { get; set; }
    }
}

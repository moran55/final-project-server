﻿using FinalProject.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static FinalProject.Common.Models.Requests;

namespace FinalProjectDAL.BusinessEntities
{
    public class ShoppingListManager
    {
        public string AddNewShoppingList(NewShoppingList spList, string userId)
        {
            using (HomeLife_FinalProjectEntities _context = new HomeLife_FinalProjectEntities())
            {
                try
                {
                    var user = _context.tbl_User.Where(x => x.UserId == new Guid(userId)).FirstOrDefault();
                    if(user.FamilyId != Guid.Empty && user.FamilyId != null)
                    {
                        var sp = _context.tbl_ShoppingList.Add(new tbl_ShoppingList()
                        {
                            ShoppingListId = Guid.NewGuid(),
                            ShoppingListName = spList.ShoppingListName,
                            IsComplete = false,
                            FamilyId = user.FamilyId
                        });
                        if (spList.ShoppingItems != null)
                        {
                            foreach (var si in spList.ShoppingItems)
                            {
                                _context.tbl_ShoppingItem.Add(new tbl_ShoppingItem()
                                {
                                    ShoppingItemId = Guid.NewGuid(),
                                    ItemName = si.ItemName,
                                    Amount = si.Quantity,
                                    TotalPrice = 0,
                                    IsComplete = false,
                                    Comment = "",
                                    ShoppingListId = sp.ShoppingListId
                                });
                            }
                        }

                        _context.SaveChanges();
                        return "";
                    }
                    else
                    {
                        return "Please Set Family First To Add New Tasks";
                    }
                   
                }
                catch (Exception)
                {
                    return "Server error - please try again later";
                }
            }
        }

        public object GetTasks(Guid UserId)
        {
            try
            {
                using (HomeLife_FinalProjectEntities _context = new HomeLife_FinalProjectEntities())
                {
                    var user = _context.tbl_User.FirstOrDefault(x => x.UserId == UserId);
                    var tasks = _context.tbl_Task.Where(x => x.UserId != Guid.Empty && x.FamilyId == user.FamilyId && x.IsRecursive == false && (x.DaysList == "" || x.DaysList == null)
                        && x.IsShoppingListTask == false).ToList();
                    if (tasks != null)
                    {
                        return tasks;
                    }
                    else {
                        return null;   
                    }
                }
            }
            catch (Exception ex)
            {
                return null;
            }

        }

        public object AddNewShoppingItem(AddNewShoppingItem data)
        {
            try
            {
                using (HomeLife_FinalProjectEntities _context = new HomeLife_FinalProjectEntities())
                {
                    var newShoppingItem = new tbl_ShoppingItem()
                    {
                        ItemName = data.ItemName,
                        Amount = int.Parse(data.Amount),
                        IsComplete = false,
                        ShoppingItemId = Guid.NewGuid(),
                        ShoppingListId = new Guid(data.ShoppingListId),
                        TotalPrice = 0
                    };

                    _context.tbl_ShoppingItem.Add(newShoppingItem);
                    _context.SaveChanges();
                    return newShoppingItem.ShoppingItemId;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public string SaveShoppingListToTask(ShoppingToTask slAndTask)
        {
            try
            {
                using (HomeLife_FinalProjectEntities _context = new HomeLife_FinalProjectEntities())
                {
                    var task = _context.tbl_Task.Where(x => x.TaskId == slAndTask.TaskId).FirstOrDefault();
                    var sl = _context.tbl_ShoppingList.Where(x => x.ShoppingListId == slAndTask.ShoppingListId).FirstOrDefault();
                    if(task!= null && sl != null)
                    {
                        if(sl.TaskId != Guid.Empty)
                        {
                            var oldTask = _context.tbl_Task.FirstOrDefault(x => x.TaskId == sl.TaskId);
                            oldTask.IsShoppingListTask = false;
                        }
                        task.IsShoppingListTask = true;
                        sl.TaskId = task.TaskId;
                        _context.SaveChanges();
                        return "";
                    }
                    return "Can't save shopping list to task";
                }
            }
            catch (Exception ex)
            {
                return "Server error - please try again later";
            }
        }

        public bool DeleteShoppingItem(string shoppingItemId)
        {
            using (HomeLife_FinalProjectEntities _context = new HomeLife_FinalProjectEntities())
            {
                try
                {
                    var shoppingItem = _context.tbl_ShoppingItem.Where(x => x.ShoppingItemId == new Guid(shoppingItemId)).FirstOrDefault();
                    _context.tbl_ShoppingItem.Remove(shoppingItem);
                    var shoppingItemsForSameList = _context.tbl_ShoppingItem.Where(x => x.ShoppingListId == shoppingItem.ShoppingListId).ToList();
                    if(shoppingItemsForSameList.Count <= 1)
                    {
                        _context.tbl_ShoppingList.Remove(_context.tbl_ShoppingList.Where(x => x.ShoppingListId == shoppingItem.ShoppingListId).FirstOrDefault());
                    }
                    _context.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {
                    return false;
                }
                
            }
        }

        public List<ShoppingList> FetchByFamilyId(Guid FamilyId)
        {
            using (HomeLife_FinalProjectEntities _context = new HomeLife_FinalProjectEntities())
            {
                List<ShoppingList> lists = new List<ShoppingList>();
                var shoppingLists = _context.tbl_ShoppingList.Where(x => x.FamilyId == FamilyId).OrderBy(x=> x.TimeStamp).ToList();
                foreach (var shoppingList in shoppingLists)
                {
                    var newShoppingList = new ShoppingList()
                    {
                        ShoppingListId = shoppingList.ShoppingListId,
                        ShoppingListName = shoppingList.ShoppingListName,
                        IsComplete = shoppingList.IsComplete,
                        TaskId = shoppingList.TaskId,
                        shoppingItems = new List<ShoppingItem>()
                    };

                    var shoppingItems = _context.tbl_ShoppingItem.Where(x => x.ShoppingListId == newShoppingList.ShoppingListId).OrderBy(x=> x.ItemName
                    ).ToList();
                    newShoppingList.shoppingItems = new List<ShoppingItem>();
                    foreach (var si in shoppingItems)
                    {
                        newShoppingList.shoppingItems.Add(new ShoppingItem() {
                            ShoppingItemId = si.ShoppingItemId,
                            ItemName = si.ItemName,
                            Amount = (float)si.Amount,
                            TotalPrice = (float)si.TotalPrice,
                            IsComplete = (bool)si.IsComplete,
                            Comment = si.Comment,
                            ShoppingListId = newShoppingList.ShoppingListId
                        });
                    }
                    lists.Add(newShoppingList);
                }
                return lists;
            }
        }

        public bool DeleteShoppingList(Guid shoppingListId)
        {
            try
            {
                using (HomeLife_FinalProjectEntities _context = new HomeLife_FinalProjectEntities())
                {
                    var siToDelete = _context.tbl_ShoppingItem.Where(x => x.ShoppingListId == shoppingListId).ToList();
                    foreach (var si in siToDelete)
                    {
                        _context.tbl_ShoppingItem.Remove(si);
                    }
                    var slToDelete = _context.tbl_ShoppingList.Where(x => x.ShoppingListId == shoppingListId).FirstOrDefault();
                    if(slToDelete.TaskId != Guid.Empty)
                    {
                        var task = _context.tbl_Task.FirstOrDefault(x => x.TaskId == slToDelete.TaskId);
                        task.IsShoppingListTask = false;
                    }
                    _context.tbl_ShoppingList.Remove(slToDelete);
                    _context.SaveChanges();
                    return true;

                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool ChangeShoppingItemsIsComplete(CompleteChange data)
        {
            try
            {
                using (HomeLife_FinalProjectEntities _context = new HomeLife_FinalProjectEntities())
                {
                    foreach (var ShoppingItemId in data.ShoppingItemsTrueIds)
                    {
                        var siIds = _context.tbl_ShoppingItem.Where(x => x.ShoppingItemId == new Guid(ShoppingItemId)).FirstOrDefault();
                        if (siIds != null)
                        {
                                siIds.IsComplete = true;
                        }
                    }
                    foreach (var ShoppingItemId in data.ShoppingItemsFalseIds)
                    {
                        var siIds = _context.tbl_ShoppingItem.Where(x => x.ShoppingItemId == new Guid(ShoppingItemId)).FirstOrDefault();
                        if (siIds != null)
                        {
                            siIds.IsComplete = false;
                        }
                    }
                    _context.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}

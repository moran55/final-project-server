﻿using FinalProject.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static FinalProject.Common.Models.Requests;

namespace FinalProjectDAL.BusinessEntities
{
    public class UserManager
    {
        public User Fetch(Guid userId)
        {
            using (HomeLife_FinalProjectEntities _context = new HomeLife_FinalProjectEntities())
            {
                List<taskForUser> tasksList = new List<taskForUser>();
                var userDb = _context.tbl_User.FirstOrDefault(x => x.UserId == userId);
                var userLoginDb = _context.tbl_UserLogin.FirstOrDefault(x => x.UserId == userId);

                var tasks = _context.tbl_Task.Where(x => x.FamilyId == userDb.FamilyId).ToList();
                var isLikeTaskByUser = _context.tbl_IsUserLikedTask.Where(x => x.UserId == userDb.UserId).ToList();

                foreach (var task in tasks)
                {
                    var tasksIsLiked = isLikeTaskByUser.Where(x => x.TaskId == task.TaskId).FirstOrDefault();
                    if(tasksIsLiked == null)
                    {
                        tasksList.Add(new taskForUser
                        {
                            TaskId = task.TaskId,
                            TaskName = task.TaskName,
                            IsLikeTask = 0
                        });
                    }
                    else
                    {
                        tasksList.Add(new taskForUser
                        {
                            TaskId = task.TaskId,
                            TaskName = task.TaskName,
                            UserId = (Guid)tasksIsLiked.UserId,
                            IsLikeTask = tasksIsLiked.IsLikeTask == true ? 1 : -1
                        });
                    }
                    
                }
                return new User()
                {
                    UserId = userDb.UserId,
                    UserName = userLoginDb.UserName,
                    //Password = userLoginDb.Password,
                    FirstName = userDb.FirstName,
                    LastName = userDb.LastName,
                    IsHasDrivingLicense = userDb.IsHadDrivingLicense,
                    IsParent = userDb.IsParent,
                    AvailableStartTask = userDb.AvailableStartTask == null ? new TimeSpan(12,00,00) : userDb.AvailableStartTask,
                    AvailableEndTask = userDb.AvailableEndTask == null ? new TimeSpan(12, 00, 00) : userDb.AvailableEndTask,
                    FamilyId = userDb.FamilyId,
                    BirthDate = userDb.BirthDate,
                    Score = userDb.Score == null ? 0 : userDb.Score,
                    TasksList = tasksList
                };
            }
        }

        public string CheckIfUserNameExist(string userName)
        {
            try
            {
                using (HomeLife_FinalProjectEntities _context = new HomeLife_FinalProjectEntities())
                {
                    var loginUser = _context.tbl_UserLogin.Where(x => x.UserName == userName).FirstOrDefault();
                    if (loginUser == null)
                        return "This User Name Not Register To APP";
                    else
                        return "";
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<User> FetchByDrivingLicense(Family family)
        {
            using (HomeLife_FinalProjectEntities _context = new HomeLife_FinalProjectEntities())
            {
                var UsersDb = _context.tbl_User.Where(x => x.FamilyId == family.FamilyId && x.IsHadDrivingLicense == true).ToList();
                List<User> users = new List<User>();
                foreach (var user in UsersDb)
                {
                    users.Add(new User()
                    {
                        UserId = user.UserId,
                        FirstName = user.FirstName,
                        LastName = user.LastName,
                        IsHasDrivingLicense = user.IsHadDrivingLicense,
                        IsParent = user.IsParent,
                        FamilyId = user.FamilyId
                    });
                }
                return users;
            }
        }

        public List<User> FetchByIsParent(Family family)
        {
            using (HomeLife_FinalProjectEntities _context = new HomeLife_FinalProjectEntities())
            {
                var parents = _context.tbl_User.Where(x => x.FamilyId == family.FamilyId && x.IsParent == true).ToList();
                List<User> users = new List<User>();
                foreach (var user in parents)
                {
                    users.Add(new User()
                    {
                        UserId = user.UserId,
                        FirstName = user.FirstName,
                        LastName = user.LastName,
                        IsHasDrivingLicense = user.IsHadDrivingLicense,
                        IsParent = user.IsParent,
                        FamilyId = user.FamilyId
                    });
                }
                return users;
            }
        }

        public List<User> FetchUsersByFamilyId(Guid familyId)
        {
            using (HomeLife_FinalProjectEntities _context = new HomeLife_FinalProjectEntities())
            {
                var UsersDb = _context.tbl_User.Where(x => x.FamilyId == familyId).ToList();
                List<User> users = new List<User>();
                foreach (var user in UsersDb)
                {
                    users.Add(new User()
                    {
                        UserId = user.UserId,
                        FirstName = user.FirstName,
                        LastName = user.LastName,
                        IsHasDrivingLicense = user.IsHadDrivingLicense,
                        IsParent = user.IsParent,
                        FamilyId = user.FamilyId
                    });
                }
                return users;
            }
        }

        public User FetchLoginUser(User user)
        {
            using (HomeLife_FinalProjectEntities _context = new HomeLife_FinalProjectEntities())
            {
                var UserLoginDb = _context.tbl_UserLogin.FirstOrDefault(x => x.UserName.ToLower() == user.UserName.ToLower());
                //var base64EncodedBytes = System.Convert.FromBase64String(user.Password);
                //var decodePassword = System.Text.Encoding.UTF8.GetString(base64EncodedBytes);

                if (UserLoginDb != null && UserLoginDb.Password == user.Password)
                {
                    user.UserId = (Guid)UserLoginDb.UserId;
                    return new User()
                    {
                        UserId = (Guid)UserLoginDb.UserId,
                        UserName = UserLoginDb.UserName,
                        Password = UserLoginDb.Password,
                        FirstName = user.FirstName,
                        LastName = user.LastName,
                        BirthDate = user.BirthDate,
                        IsHasDrivingLicense = user.IsHasDrivingLicense,
                        IsParent = user.IsParent,
                        FamilyId = user.FamilyId

                    };
                }
                return null;
            }
        }

        public UserLogin FetchLoginUser(Guid userId)
        {
            using (HomeLife_FinalProjectEntities _context = new HomeLife_FinalProjectEntities())
            {
                var userLogin = _context.tbl_UserLogin.Where(x => x.UserId == userId).First();
                if (userLogin != null)
                {
                    return new UserLogin()
                    {
                        UserId = (Guid)userLogin.UserId,
                        UserName = userLogin.UserName,
                        Password = userLogin.Password,

                    };
                }
                return null;
            }
        }

        public String AddNewUser(NewUser user)
        {
            using (HomeLife_FinalProjectEntities _context = new HomeLife_FinalProjectEntities())
            {
                try
                {
                    var loginUser = _context.tbl_UserLogin.Where(x => x.UserName == user.UserName).FirstOrDefault();
                    if (loginUser != null)
                    {
                        return "This User Name Already Exist";
                    }

                    var birthDate = DateTime.MinValue;
                    if (user.BirthDate != "")
                    {
                        var dateSplit = user.BirthDate.Split('-');
                        int year = int.Parse(dateSplit[0]);
                        int month = int.Parse(dateSplit[1]);
                        int day = int.Parse(dateSplit[2]);
                        birthDate = new DateTime(year, month, day);
                    }
                    var availableStartTask = TimeSpan.MinValue;
                    var availableEndTask = TimeSpan.MinValue;
                    if (user.AvailableStartTask != "" && user.AvailableEndTask != "")
                    {
                        var startTime = user.AvailableStartTask.Split(':');
                        availableStartTask = new TimeSpan(int.Parse(startTime[0]), int.Parse(startTime[1]), 00);
                        var endTime = user.AvailableEndTask.Split(':');
                        availableEndTask = new TimeSpan(int.Parse(endTime[0]), int.Parse(endTime[1]), 00);
                    }
                    
                    var newUser = _context.tbl_User.Add(new tbl_User
                    {
                        UserId = Guid.NewGuid(),
                        FirstName = user.FirstName,
                        LastName = user.LastName,
                        BirthDate = birthDate,
                        AvailableStartTask = availableStartTask == TimeSpan.MinValue ? TimeSpan.Zero : availableStartTask,
                        AvailableEndTask = availableEndTask == TimeSpan.MinValue ? TimeSpan.Zero : availableEndTask,
                        IsHadDrivingLicense = user.IsHasDrivingLiecence,
                        Score = 0,
                        IsParent = user.IsParent
                    });

                    var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(user.Password);
                    var encodePassword = System.Convert.ToBase64String(plainTextBytes);
                    

                    _context.tbl_UserLogin.Add(new tbl_UserLogin
                    {
                        UserLoginId = Guid.NewGuid(),
                        UserId = newUser.UserId,
                        UserName = user.UserName,
                        Password = encodePassword
                    });

                    _context.SaveChanges();
                    return "";
                }
                catch (Exception ex)
                {
                    return "Something went wrong, please try again later";
                }

            }
        }

        public string UpdateUser(User user)
        {
            using (HomeLife_FinalProjectEntities _context = new HomeLife_FinalProjectEntities())
            {
                try
                {
                    var userToEdit = _context.tbl_User.Where(x => x.UserId == user.UserId).FirstOrDefault();
                    if (userToEdit != null)
                    {
                        userToEdit.FirstName = user.FirstName == null || user.FirstName.Trim() == "" ? userToEdit.FirstName : user.FirstName;
                        userToEdit.LastName = user.LastName == null || user.LastName.Trim() == "" ? userToEdit.LastName : user.LastName;
                        userToEdit.IsParent = user.IsParent;
                        userToEdit.IsHadDrivingLicense = user.IsHasDrivingLicense;
                        userToEdit.AvailableStartTask = user.AvailableStartTask;
                        userToEdit.AvailableEndTask = user.AvailableEndTask;
                        userToEdit.BirthDate = user.BirthDate;

                        _context.SaveChanges();
                        return "";
                    }
                    return "User is undifiend";
                }
                catch (Exception)
                {
                    return "Server Error - Please try again later";
                }

            }
        }
    }
}

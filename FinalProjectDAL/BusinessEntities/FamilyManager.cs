﻿using FinalProject.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static FinalProject.Common.Models.Requests;

namespace FinalProjectDAL.BusinessEntities
{
    public class FamilyManager
    {
        public Family Fetch(Guid familyId)
        {
            using (HomeLife_FinalProjectEntities _context = new HomeLife_FinalProjectEntities())
            {
                var familyDb =  _context.tbl_Family.FirstOrDefault(x => x.FamilyId == familyId);
                return new Family()
                {
                    FamilyId = familyDb.FamilyId,
                    FamilyName = familyDb.FamilyName
                };
            }
        }

        public List<User> FetchByUserID(Guid userId)
        {
            using (HomeLife_FinalProjectEntities _context = new HomeLife_FinalProjectEntities())
            {
                List<User> ul = new List<User>();
                
                var specUser = _context.tbl_User.Where(x => x.UserId == userId).FirstOrDefault();
                List<tbl_User> familyUsers = new List<tbl_User>();
                if (specUser.FamilyId != Guid.Empty && specUser.FamilyId != null)
                {
                   familyUsers = _context.tbl_User.Where(x => x.FamilyId == specUser.FamilyId).ToList();
                    foreach (var user in familyUsers)
                    {
                        var newUser = new User()
                        {
                            UserId = user.UserId,
                            FirstName = user.FirstName,
                            LastName = user.LastName,
                            IsParent = user.IsParent,
                            Score = user.Score,
                            Age = (DateTime.Now.Year - user.BirthDate.Value.Date.Year) > 0 ? DateTime.Now.Year - user.BirthDate.Value.Year : 0 ,
                            IsHasDrivingLicense = user.IsHadDrivingLicense,
                            FamilyId = user.FamilyId
                        };
                        ul.Add(newUser);
                    }
                }
                else
                {
                    var newUser = new User()
                    {
                        UserId = specUser.UserId,
                        FirstName = specUser.FirstName,
                        LastName = specUser.LastName,
                        IsParent = specUser.IsParent,
                        IsHasDrivingLicense = specUser.IsHadDrivingLicense,
                        FamilyId = specUser.FamilyId
                    };
                    ul.Add(newUser);
                }
                

                
                return ul;
                
            }
        }

        public string AddUserAsFamilyMember(NewMember newMember)
        {
            try
            {
                using (HomeLife_FinalProjectEntities _context = new HomeLife_FinalProjectEntities())
                {

                    var userLogin = _context.tbl_UserLogin.FirstOrDefault(x => x.UserName == newMember.email);
                    if (userLogin == null)
                    {
                        return "User not exist";
                    }
                    var user = _context.tbl_User.FirstOrDefault(x => x.UserId == userLogin.UserId);
                    
                    if(user.FamilyId != null && user.FamilyId != Guid.Empty)
                    {
                        return "User already exist in another family";
                    }

                    user.FamilyId = new Guid(newMember.familyId);
                    _context.SaveChanges();
                    return "";
                }
            }
            catch (Exception)
            {
                return "Server error - please try again later";
            }
        }

        public string setFamilyName(Guid userId, string familyName)
        {
            try
            {
                using (HomeLife_FinalProjectEntities _context = new HomeLife_FinalProjectEntities())
                {
                    var user = _context.tbl_User.Where(x => x.UserId == userId).FirstOrDefault();
                    if(user != null)
                    {
                        if(familyName != "")
                        {
                            var family = new tbl_Family
                            {
                                FamilyId = Guid.NewGuid(),
                                FamilyName = familyName
                            };
                            _context.tbl_Family.Add(family);
                            user.FamilyId = family.FamilyId;
                            _context.SaveChanges();
                        }
                        else
                        {
                            return "Family name can't be empty";
                        }
                    }
                    else
                    {
                        return "User is Undifiend - try again later";
                    }
                    return "";
                }
            }
            catch (Exception ex)
            {
                return "Server error - please try again later";
            }
        }

        public string GetFamilyName(Guid userId)
        {
            try
            {
                using (HomeLife_FinalProjectEntities _context = new HomeLife_FinalProjectEntities())
                {
                    var user = _context.tbl_User.Where(x => x.UserId == userId).FirstOrDefault();
                    var family = _context.tbl_Family.Where(x => x.FamilyId == user.FamilyId).FirstOrDefault();
                    if (family == null)
                    {
                        return "";
                    }
                    else
                    {
                        return family.FamilyName;
                    }
                }
            }
            catch (Exception ex)
            {
                return "Server error - please try again later";
            }
        }
    }
}

﻿using FinalProject.Common;
using FinalProject.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static FinalProject.Common.Models.Requests;

namespace FinalProjectDAL.BusinessEntities
{
    public class TaskManager
    {
        public Task Fetch(Guid TaskId)
        {
            using (HomeLife_FinalProjectEntities _context = new HomeLife_FinalProjectEntities())
            {
                var task = _context.tbl_Task.FirstOrDefault(x => x.TaskId == TaskId);

                return new Task()
                {
                    TaskId = task.TaskId,
                    TaskName = task.TaskName,
                    IsDrivingLicenseNeeded = task.IsDrivingLicenseNeeded,
                    IsParentNeeded = task.IsParentNeeded,
                    IsRecursive = task.IsRecursive,
                    IsComplete = task.IsComplete,
                    UserId = task.UserId
                };
            }
        }

        public List<Task> FetchTasksByUserId(Guid UserId)
        {
            using (HomeLife_FinalProjectEntities _context = new HomeLife_FinalProjectEntities())
            {
                List<Task> tasksList = new List<Task>();
                List<tbl_User> users = null;
                List<Guid> usersIds = null;
                var user = _context.tbl_User.Where(x => x.UserId == UserId).FirstOrDefault();
                if (user.FamilyId != Guid.Empty && user.FamilyId != null)
                {
                    Guid familyId = (Guid)user.FamilyId;
                    users = _context.tbl_User.Where(x => x.FamilyId == familyId).ToList();
                    usersIds = users.Select(x => x.UserId).ToList();
                }
                List<tbl_Task> tasks = null;
                if (users == null)
                    tasks = _context.tbl_Task.Where(x => x.UserId == UserId).OrderByDescending(x => x.TimeStamp).ToList();
                else
                {
                    tasks = _context.tbl_Task.Where(x => usersIds.Contains((Guid)x.UserId)).ToList();

                    var familyNoUserTasks = _context.tbl_Task.Where(x => x.FamilyId == user.FamilyId && x.UserId == Guid.Empty).ToList();
                    tasks.AddRange(familyNoUserTasks);
                    tasks = tasks.OrderByDescending(x => x.TimeStamp).ToList();
                }


                foreach (var task in tasks)
                {
                    var userForTask = users.Where(x => x.UserId == task.UserId).FirstOrDefault();
                    String shoppingListName = String.Empty;
                    if ((bool)task.IsShoppingListTask)
                    {
                        var shoppingList = _context.tbl_ShoppingList.Where(x => x.TaskId == task.TaskId).FirstOrDefault();
                        if (shoppingList != null)
                            shoppingListName = shoppingList.ShoppingListName;
                    }
                    var newTask = new Task()
                    {
                        TaskId = task.TaskId,
                        TaskName = task.TaskName,
                        IsDrivingLicenseNeeded = task.IsDrivingLicenseNeeded,
                        IsParentNeeded = task.IsParentNeeded,
                        IsRecursive = task.IsRecursive,
                        IsComplete = task.IsComplete,
                        Days = task.DaysList == null ? "" : task.DaysList,
                        Description = task.Description,
                        TaskScore = task.TaskScore,
                        UserFullName = userForTask == null ? "Multiple Users..." : userForTask.FirstName + " " + userForTask.LastName,
                        UserId = task.UserId,
                        IsShoppingListTask = task.IsShoppingListTask == null ? false : (bool)task.IsShoppingListTask,
                        ShoppingListName = shoppingListName
                    };

                    var taskHours = _context.tbl_TaskHours.Where(x => x.TaskId == task.TaskId).OrderBy(x => x.StartTask).ToList();
                    newTask.TaskHours = new List<TaskHours>();
                    foreach (var th in taskHours)
                    {
                        var taskHoursFromJoiner = _context.tbl_TaskHoursByUserJoiner.FirstOrDefault(x => x.TaskHoursId == th.TaskHoursId);
                        newTask.TaskHours.Add(new TaskHours()
                        {
                            TaskHoursId = th.TaskHoursId,
                            startTask = th.StartTask,
                            EndTask = th.EndTask,
                            TaskId = newTask.TaskId,
                            isComplete = th.IsComplete == null ? false : (bool)th.IsComplete,
                            UserFromJoiner = taskHoursFromJoiner == null ? Guid.Empty : (Guid)taskHoursFromJoiner.UserId
                        });
                    }
                    tasksList.Add(newTask);
                }
                return tasksList;
            }
        }

        public string UpdateTasksIsLikeByUser(List<TasksListForIsLike> tasksIsLike, string userId)
        {
            try
            {
                using (HomeLife_FinalProjectEntities _context = new HomeLife_FinalProjectEntities())
                {
                    Guid id = new Guid(userId);
                    List<tbl_IsUserLikedTask> tasksLikeToRemove = new List<tbl_IsUserLikedTask>();
                    foreach (var taskLike in tasksIsLike)
                    {
                        var IsTaskExist = _context.tbl_IsUserLikedTask.Where(x => x.TaskId == new Guid(taskLike.TaskId) && x.UserId == id).FirstOrDefault();
                        if (IsTaskExist != null)
                        {
                            if (taskLike.IsLike == 1)
                                IsTaskExist.IsLikeTask = true;
                            else if (taskLike.IsLike == -1)
                                IsTaskExist.IsLikeTask = false;
                            else
                            {
                                tasksLikeToRemove.Add(IsTaskExist);
                            }
                        }
                        else
                        {
                            if (taskLike.IsLike != 0)
                            {
                                _context.tbl_IsUserLikedTask.Add(new tbl_IsUserLikedTask()
                                {
                                    IsLikeTaskId = Guid.NewGuid(),
                                    TaskId = new Guid(taskLike.TaskId),
                                    UserId = id,
                                    IsLikeTask = taskLike.IsLike == 1 ? true : false
                                });
                            }
                        }
                    }
                    if (tasksLikeToRemove.Count > 0)
                    {
                        _context.tbl_IsUserLikedTask.RemoveRange(tasksLikeToRemove);
                    }
                    _context.SaveChanges();
                    return "";
                }
            }
            catch (Exception)
            {
                return "Server Error - Please Try Again Later.";
            }

        }

        public string DeleteTask(RemoveTask taskId)
        {
            try
            {
                using (HomeLife_FinalProjectEntities _context = new HomeLife_FinalProjectEntities())
                {
                    var task = _context.tbl_Task.Where(x => x.TaskId == new Guid(taskId.TaskId)).FirstOrDefault();
                    var taskHours = _context.tbl_TaskHours.Where(x => x.TaskId == new Guid(taskId.TaskId)).ToList();
                    var taskHourIds = taskHours.Select(x => x.TaskHoursId).ToList();
                    var taskHoursFromJoiner = _context.tbl_TaskHoursByUserJoiner.Where(x => taskHourIds.Contains((Guid)x.TaskHoursId)).ToList();
                    if ((bool)task.IsShoppingListTask)
                    {
                        var shoppingList = _context.tbl_ShoppingList.FirstOrDefault(x=> x.TaskId == task.TaskId);
                        shoppingList.TaskId = Guid.Empty;
                    }
                    _context.tbl_TaskHoursByUserJoiner.RemoveRange(taskHoursFromJoiner);
                    _context.tbl_TaskHours.RemoveRange(taskHours);
                    _context.tbl_Task.Remove(task);
                    _context.SaveChanges();
                    return "";
                }
            }
            catch (Exception ex)
            {
                return "Server Error - Please Try Again Later.";
            }
        }

        public List<Task> FetchByDrivingLicenseNeeded(Guid userId)
        {
            using (HomeLife_FinalProjectEntities _context = new HomeLife_FinalProjectEntities())
            {
                var tasksDb = _context.tbl_Task.Where(x => x.IsDrivingLicenseNeeded == true).ToList();
                List<Task> tasks = new List<Task>();
                foreach (var task in tasksDb)
                {
                    tasks.Add(new Task()
                    {
                        TaskId = task.TaskId,
                        TaskName = task.TaskName,
                        IsDrivingLicenseNeeded = task.IsDrivingLicenseNeeded,
                        IsParentNeeded = task.IsParentNeeded,
                        IsRecursive = task.IsRecursive,
                        IsComplete = task.IsComplete,
                        UserId = task.UserId
                    });
                }
                return tasks;
            }
        }

        public List<Task> FetchByParentNeeded(Guid userId)
        {
            using (HomeLife_FinalProjectEntities _context = new HomeLife_FinalProjectEntities())
            {
                var tasksDb = _context.tbl_Task.Where(x => x.IsParentNeeded == true).ToList();
                List<Task> tasks = new List<Task>();
                foreach (var task in tasksDb)
                {
                    tasks.Add(new Task()
                    {
                        TaskId = task.TaskId,
                        TaskName = task.TaskName,
                        IsDrivingLicenseNeeded = task.IsDrivingLicenseNeeded,
                        IsParentNeeded = task.IsParentNeeded,
                        IsRecursive = task.IsRecursive,
                        IsComplete = task.IsComplete,
                        UserId = task.UserId
                    });
                }
                return tasks;
            }
        }

        public string AddNewTask(NewTask task, String loginUserId)
        {
            using (HomeLife_FinalProjectEntities _context = new HomeLife_FinalProjectEntities())
            {
                task.endTaskDate = task.startTaskDate;
                Guid userId;
                tbl_User user;
                if (task.UserId == "" || task.UserId == "0")
                {
                    userId = Guid.Empty;
                    user = _context.tbl_User.Where(x => x.UserId == new Guid(loginUserId)).FirstOrDefault();
                }
                else
                {
                    userId = new Guid(task.UserId);
                    user = _context.tbl_User.Where(x => x.UserId == userId).FirstOrDefault();
                }


                if (user.FamilyId == null || user.FamilyId == Guid.Empty)
                {
                    return "Please Set Family First To Add New Tasks";
                }
                if (task.IsDrivingLicenseNeeded && task.UserId != "" && task.UserId != "0" )
                {
                    if (!(bool)user.IsHadDrivingLicense)
                    {
                        return "Driving liecence needed!";
                    }
                }
                if (task.IsParentNeeded && task.UserId != "" && task.UserId != "0")
                {
                    if (!(bool)user.IsParent)
                    {
                        return "Parent needed!";
                    }
                }
                try
                {
                    var newTask = _context.tbl_Task.Add(new tbl_Task
                    {
                        TaskId = Guid.NewGuid(),
                        TaskName = task.TaskName,
                        Description = task.Description,
                        IsDrivingLicenseNeeded = task.IsDrivingLicenseNeeded,
                        IsParentNeeded = task.IsParentNeeded,
                        IsRecursive = task.IsRecursive,
                        DaysList = "",
                        IsComplete = false,
                        TaskScore = task.UserId != "" && task.UserId != "0" ? 0 : task.TaskScore,
                        UserId = task.UserId == "" || task.UserId == "0" ? Guid.Empty :  user.UserId,
                        FamilyId = user.FamilyId,
                        IsShoppingListTask = false
                    });

                    var startTask = StringToDateTime(task.startTaskDate, task.startTaskTime);
                    var endTask = StringToDateTime(task.endTaskDate, task.endTaskTime);

                    if (startTask > endTask)
                    {
                        return "Start task must be greater than the end task";
                    }

                    if((user != null && newTask.UserId != Guid.Empty) && !(user.AvailableStartTask <= startTask.TimeOfDay && user.AvailableEndTask >= endTask.TimeOfDay))
                    {
                        return "user is not available in task hours";
                    }

                    if (!IsUserBusy(startTask, endTask, user) && task.UserId != "" && task.UserId != "0")
                    {
                        return "User is Busy, Try in another date";
                    }

                    if (task.IsRecursive)
                    {
                        List<int> days = new List<int>();

                        if (task.Days.sunday) { days.Add(0); }
                        if (task.Days.monday) { days.Add(1); }
                        if (task.Days.tuesday) { days.Add(2); }
                        if (task.Days.wednesday) { days.Add(3); }
                        if (task.Days.thursday) { days.Add(4); }
                        if (task.Days.friday) { days.Add(5); }
                        if (task.Days.saturday) { days.Add(6); }

                        newTask.DaysList = string.Join(",", days);

                        for (int i = 0; i < 4; i++)
                        {
                            foreach (var day in days)
                            {
                                var isDayAdded = false;
                                while (!isDayAdded)
                                {
                                    var dayOfWeek = endTask.DayOfWeek;
                                    if ((int)dayOfWeek == day)
                                    {
                                        _context.tbl_TaskHours.Add(new tbl_TaskHours
                                        {
                                            TaskHoursId = Guid.NewGuid(),
                                            StartTask = startTask,
                                            EndTask = endTask,
                                            TaskId = newTask.TaskId,
                                            IsComplete = false
                                        });
                                        isDayAdded = true;
                                    }
                                    startTask = startTask.AddDays(1);
                                    endTask = endTask.AddDays(1);
                                }
                            }
                        }
                    }
                    else
                    {
                        tbl_TaskHours FirstTaskHours = null;
                        if (startTask != null && endTask != null)
                        {
                            FirstTaskHours = _context.tbl_TaskHours.Add(new tbl_TaskHours
                            {
                                TaskHoursId = Guid.NewGuid(),
                                StartTask = startTask,
                                EndTask = endTask,
                                TaskId = newTask.TaskId,
                                IsComplete = false
                            });
                        }
                    }

                    _context.SaveChanges();

                    if (task.UserId == "" || task.UserId == "0")
                    {
                        return AlgorithmTaskByUsers(newTask.TaskId.ToString(), user.UserId.ToString());
                    }

                    //if no user(s) match delete the task(s)
                    
                    return "";
                }
                catch (Exception ex)
                {
                    return "Server Error - Please Try Again Later";
                }

            }
        }

        public string UpdateTask(UpdateTask task, String loginUserId)
        {
            using (HomeLife_FinalProjectEntities _context = new HomeLife_FinalProjectEntities())
            {
                try
                {
                    var taskHoursUserJoinerToRemove = new List<tbl_TaskHoursByUserJoiner>();
                    var taskToEdit = _context.tbl_Task.Where(x => x.TaskId == new Guid(task.TaskId)).First();
                    var userLogin = _context.tbl_User.FirstOrDefault(x => x.UserId ==  new Guid(loginUserId));
                    bool isChangeDaysList = false;
                    tbl_User taskToEdituser = null;
                    string taskDays = String.Empty;
                    if (task.Days.Length > 0)
                    {
                        if (task.Days.Length == 1 && task.Days[0] == "")
                        {
                            taskDays = String.Empty;
                        }
                        else
                        {
                            taskDays = string.Join(",", task.Days);
                        }
                    }

                    if (taskToEdit != null)
                    {
                        var isTaskWasAuto = false;
                        if(taskToEdit.UserId == Guid.Empty)
                        {
                            isTaskWasAuto = true;
                        }
                        taskToEdit.TaskName = task.TaskName;
                        taskToEdit.Description = task.Description;

                        // check user is parent or have DL when try to change user in task edit
                        taskToEdituser = _context.tbl_User.FirstOrDefault(x => x.UserId.ToString() == task.UserId);
                        if (taskToEdituser != null)
                        {
                            if (task.IsDrivingLicenseNeeded)
                            {
                                if (!(bool)taskToEdituser.IsHadDrivingLicense)
                                {
                                    return "Can't change to picked user - beacuse he do not have a driving license";
                                }
                            }
                            if (task.IsParentNeeded)
                            {
                                if (!(bool)taskToEdituser.IsParent)
                                {
                                    return "Can't change to picked user - beacuse he do not a parent";
                                }
                            }
                        }

                        if (taskToEdit.UserId == Guid.Empty && task.UserId != Guid.Empty.ToString() && task.UserId != "0" && task.UserId != "") //change from auto to one person task
                        {
                            var taskHours = _context.tbl_TaskHours.Where(x => x.TaskId == taskToEdit.TaskId).Select(x => x.TaskHoursId).ToList();
                            taskHoursUserJoinerToRemove = _context.tbl_TaskHoursByUserJoiner.Where(x => taskHours.Contains((Guid)x.TaskHoursId)).ToList();
                            //_context.tbl_TaskHoursByUserJoiner.RemoveRange(taskHoursUserJoiner);
                            //_context.SaveChanges();
                        } 

                        taskToEdit.UserId = task.UserId != "-1" ? (task.UserId == "0" || task.UserId == "" ? taskToEdit.UserId : new Guid(task.UserId)) : Guid.Empty;

                        if (task.IsDrivingLicenseNeeded && taskToEdit.IsDrivingLicenseNeeded != task.IsDrivingLicenseNeeded && isTaskWasAuto)
                        {
                            var ans = checkIfAllUsersHasDL(taskToEdit.TaskId, (Guid)taskToEdit.UserId);
                            if(ans == false)
                            {
                                return "Can't change driving licence - one or more participants do not have driving license";
                            }
                            else
                            {
                                taskToEdit.IsDrivingLicenseNeeded = task.IsDrivingLicenseNeeded;
                            }
                        }
                        if(taskToEdit.IsDrivingLicenseNeeded != task.IsDrivingLicenseNeeded && !isTaskWasAuto)
                        {
                            taskToEdit.IsDrivingLicenseNeeded = task.IsDrivingLicenseNeeded;
                        }

                        if (task.IsParentNeeded && taskToEdit.IsParentNeeded != task.IsParentNeeded && isTaskWasAuto)
                        {
                            var ans = checkIfAllUsersHasIP(taskToEdit.TaskId, (Guid)taskToEdit.UserId);
                            if (ans == false)
                            {
                                return "Can't change is parent - one or more participants do not a parent";
                            }
                            else
                            {
                                taskToEdit.IsParentNeeded = task.IsParentNeeded;
                            }
                        }
                        if (taskToEdit.IsParentNeeded != task.IsParentNeeded && !isTaskWasAuto)
                        {
                            taskToEdit.IsParentNeeded = task.IsParentNeeded;
                        }

                        taskToEdit.DaysList = taskToEdit.DaysList == null ? "" : taskToEdit.DaysList;
                        if (!taskToEdit.DaysList.Equals(taskDays))
                        {
                            taskToEdit.DaysList = taskDays;
                            isChangeDaysList = true;
                        }
                    }

                    _context.SaveChanges();

                    if (task.startTaskDate != "" && task.startTaskTime != "" && task.endTaskDate != "" && task.endTaskTime != "")
                    {
                        var taskHoutsToEdit = _context.tbl_TaskHours.Where(x => x.TaskId == new Guid(task.TaskId)).ToList();
                        var startTask = StringToDateTime(task.startTaskDate, task.startTaskTime);
                        var endTask = StringToDateTime(task.endTaskDate, task.endTaskTime);

                        if(startTask > endTask)
                        {
                            return "Can't change start/end task - start is grather then end in task datetime";
                        }

                        
                        if (taskToEdituser != null  && !(taskToEdituser.AvailableStartTask <= startTask.TimeOfDay && taskToEdituser.AvailableEndTask >= endTask.TimeOfDay))
                        {
                            return "user is not available in task hours";
                        }

                        bool isChangeTaskHours = true;
                        
                        taskHoutsToEdit = taskHoutsToEdit.OrderBy(x => x.StartTask).ToList();
                        if (taskHoutsToEdit.Count > 0 && taskHoutsToEdit.First().StartTask == startTask && taskHoutsToEdit.First().EndTask == endTask)
                        {
                            isChangeTaskHours = false;
                        }

                        if (taskHoutsToEdit != null && taskHoutsToEdit.Count > 0)
                        {
                            if (isChangeTaskHours || isChangeDaysList) {
                                if (taskHoutsToEdit.Count == 1 && (task.Days.Length == 0 || taskDays == String.Empty))
                                {
                                    taskHoutsToEdit.FirstOrDefault().StartTask = startTask;
                                    taskHoutsToEdit.FirstOrDefault().EndTask = endTask;
                                }
                                else
                                {
                                    _context.tbl_TaskHours.RemoveRange(taskHoutsToEdit);
                                    _context.SaveChanges();
                                    for (int i = 0; i < 4; i++)
                                    {
                                        if (task.Days.Length == 0)
                                        {
                                            _context.tbl_TaskHours.Add(new tbl_TaskHours
                                            {
                                                TaskHoursId = Guid.NewGuid(),
                                                StartTask = startTask,
                                                EndTask = endTask,
                                                TaskId = taskToEdit.TaskId,
                                                IsComplete = false
                                            });
                                            break;
                                        }
                                        foreach (var day in task.Days)
                                        {
                                            var isDayAdded = false;
                                            while (!isDayAdded)
                                            {
                                                var dayOfWeek = endTask.DayOfWeek;
                                                if ((int)dayOfWeek == int.Parse(day))
                                                {
                                                    _context.tbl_TaskHours.Add(new tbl_TaskHours
                                                    {
                                                        TaskHoursId = Guid.NewGuid(),
                                                        StartTask = startTask,
                                                        EndTask = endTask,
                                                        TaskId = taskToEdit.TaskId,
                                                        IsComplete = false
                                                    });
                                                    isDayAdded = true;
                                                }
                                                startTask = startTask.AddDays(1);
                                                endTask = endTask.AddDays(1);
                                            }
                                        }
                                    }
                                    if ((bool)taskToEdit.IsComplete)
                                    {
                                        taskToEdit.IsComplete = false;
                                    }
                                }
                            }
                        }
                        else
                        {
                            _context.tbl_TaskHours.Add(new tbl_TaskHours()
                            {
                                TaskHoursId = Guid.NewGuid(),
                                StartTask = startTask,
                                EndTask = endTask,
                                TaskId = taskToEdit.TaskId,
                                IsComplete = false
                            });
                        }

                        if (!isChangeTaskHours && !isChangeDaysList)
                        {
                            tbl_User user;
                            foreach (var complete in task.isCompleteList)
                            {
                                var taskToSetIsComplete = taskHoutsToEdit.FirstOrDefault(x => x.TaskHoursId == new Guid(complete.TaskHoursId));
                                if (taskToSetIsComplete != null)
                                {
                                    var userDoTask = _context.tbl_TaskHoursByUserJoiner.FirstOrDefault(x => x.TaskHoursId == taskToSetIsComplete.TaskHoursId);
                                    if (userDoTask != null)
                                    {
                                        user = _context.tbl_User.Where(x => x.UserId == userDoTask.UserId).FirstOrDefault();
                                    }
                                    else
                                    {
                                        user = _context.tbl_User.Where(x => x.UserId == taskToEdit.UserId).FirstOrDefault();
                                    }

                                    if (user != null)
                                    {
                                        if (user.Score == null)
                                            user.Score = 0;
                                        if (!complete.IsComplete && taskToSetIsComplete.IsComplete != complete.IsComplete)
                                            user.Score -= taskToEdit.TaskScore;
                                        else
                                            user.Score += taskToEdit.TaskScore;
                                        taskToSetIsComplete.IsComplete = complete.IsComplete;
                                    }
                                }
                            }

                            //set isComplete on task after all task hours is complete
                            var taskHoursForCheckIfComplete = taskHoutsToEdit.Where(x => x.IsComplete == false || x.IsComplete == null).ToList();
                            if (taskHoursForCheckIfComplete.Count == 0)
                                taskToEdit.IsComplete = true;
                            else
                                taskToEdit.IsComplete = false;
                        }

                    }
                    if (task.UserId == "" || task.UserId == "0")
                    {
                        var msg = AlgorithmTaskByUsers(taskToEdit.TaskId.ToString(), userLogin.UserId.ToString());
                        if(msg == "")
                        {
                            _context.tbl_TaskHoursByUserJoiner.RemoveRange(taskHoursUserJoinerToRemove);
                            _context.SaveChanges();
                            return msg;
                        }
                        return msg;
                    }

                    _context.SaveChanges();
                    return "";
                }
                catch (Exception ex)
                {
                    return "Server Error - Please Try Again Later";
                }

            }
        }

        public bool checkIfAllUsersHasDL(Guid taskId, Guid userId)
        {
            using (HomeLife_FinalProjectEntities _context = new HomeLife_FinalProjectEntities())
            {
                var task = _context.tbl_Task.First(x => x.TaskId == taskId);
                var taskHoursIds = _context.tbl_TaskHours.Where(x => x.TaskId == task.TaskId).Select(x => x.TaskHoursId).ToList();
                var userIdsFromjoiner = _context.tbl_TaskHoursByUserJoiner.Where(x => taskHoursIds.Contains((Guid)x.TaskHoursId)).Select(x=> x.UserId).ToList();
                Guid id = Guid.Empty;
                if (userId != Guid.Empty)
                {
                    id = userId;
                }
                var users = _context.tbl_User.Where(x => x.UserId == id || userIdsFromjoiner.Contains(x.UserId)).ToList();

                foreach (var user in users)
                {
                    if (!(bool)user.IsHadDrivingLicense)
                        return false;
                }

            }
            return true;

        }

        public bool checkIfAllUsersHasIP(Guid taskId, Guid userId)
        {
            using (HomeLife_FinalProjectEntities _context = new HomeLife_FinalProjectEntities())
            {
                var task = _context.tbl_Task.First(x => x.TaskId == taskId);
                var taskHoursIds = _context.tbl_TaskHours.Where(x => x.TaskId == task.TaskId).Select(x => x.TaskHoursId).ToList();
                var userIdsFromjoiner = _context.tbl_TaskHoursByUserJoiner.Where(x => taskHoursIds.Contains((Guid)x.TaskHoursId)).Select(x => x.UserId).ToList();
                Guid id = Guid.Empty;
                if(userId != Guid.Empty)
                {
                    id = userId;
                }
                var users = _context.tbl_User.Where(x => x.UserId == id || userIdsFromjoiner.Contains(x.UserId)).ToList();

                foreach (var user in users)
                {
                    if (!(bool)user.IsParent)
                        return false;
                }

            }
            return true;

        }


        public bool UpdateTaskByData(Guid taskId, String taskName, Guid userId)
        {
            try
            {
                using (HomeLife_FinalProjectEntities _context = new HomeLife_FinalProjectEntities())
                {
                    var task = _context.tbl_Task.Where(x => x.TaskId == taskId).FirstOrDefault();
                    task.TaskName = taskName;
                    task.UserId = userId;
                    _context.SaveChanges();
                }

                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }

        public DateTime StringToDateTime(string startTaskDate, string startTaskTime)
        {

            var startDateArr = startTaskDate.Split('-');
            var startTimeArr = startTaskTime.Split(':');
            return new DateTime(int.Parse(startDateArr[0]), int.Parse(startDateArr[1]), int.Parse(startDateArr[2]), int.Parse(startTimeArr[0]), int.Parse(startTimeArr[1]), 00);
        }

        public void DeleteTask(List<Guid> taskHoursIds, List<tbl_TaskHours> taskHours, tbl_Task task)
        {
            try
            {
                using (HomeLife_FinalProjectEntities _context = new HomeLife_FinalProjectEntities())
                {
                    var tempTaskHoursByUserJoiner = _context.tbl_TaskHoursByUserJoiner.Where(x => taskHoursIds.Contains((Guid)x.TaskHoursId)).ToList();
                    _context.tbl_TaskHoursByUserJoiner.RemoveRange(tempTaskHoursByUserJoiner);
                    _context.tbl_TaskHours.RemoveRange(taskHours);
                    _context.tbl_Task.Remove(task);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public string AlgorithmTaskByUsers(String TaskId, String UserId)
        {
            try
            {
                using (HomeLife_FinalProjectEntities _context = new HomeLife_FinalProjectEntities())
                {
                    var task = _context.tbl_Task.Where(x => x.TaskId == new Guid(TaskId)).FirstOrDefault();
                    var taskHours = _context.tbl_TaskHours.Where(x => x.TaskId == new Guid(TaskId)).ToList();
                    var taskHoursIds = taskHours.Select(x => x.TaskHoursId).ToList(); 
                    var user = _context.tbl_User.Where(x => x.UserId == new Guid(UserId)).FirstOrDefault();
                    var familyUsers = _context.tbl_User.Where(x => x.FamilyId == user.FamilyId).ToList();

                    var canDoTaskUsers = familyUsers;
                    if ((bool)task.IsParentNeeded)
                    {
                        familyUsers = familyUsers.Where(x => x.IsParent == true).ToList();
                        if (familyUsers.Count == 0)
                        {
                            DeleteTask(taskHoursIds, taskHours, task);
                            return "Parent needed - no user find match.";
                        }
                    }

                    if ((bool)task.IsDrivingLicenseNeeded)
                    {
                        familyUsers = familyUsers.Where(x => x.IsHadDrivingLicense == true).ToList();
                        if (familyUsers.Count == 0) {
                            DeleteTask(taskHoursIds, taskHours, task);
                            return "Driving license needed - no user find match.";
                        }
                    }
                    var taskStartTime = taskHours.FirstOrDefault().StartTask.Value.TimeOfDay;
                    var taskEndTime = taskHours.FirstOrDefault().EndTask.Value.TimeOfDay;
                    familyUsers = familyUsers.Where(x => x.AvailableStartTask <= taskStartTime && x.AvailableEndTask >= taskEndTime).ToList();
                    if (familyUsers.Count == 0) {
                        DeleteTask(taskHoursIds, taskHours, task);
                        return "Users busy in this date & time - no user find match.";
                    }

                    //-----------------------------------------------------
                    var noMatch = 0;
                    var familyusersIds = familyUsers.Select(x => x.UserId).ToList();
                    foreach (var th in taskHours)
                    {
                        List<tbl_TaskHours> tasksHoursForUser = new List<tbl_TaskHours>();
                        List<tbl_TaskHours> userTHToRemove = new List<tbl_TaskHours>();
                        List<tbl_User> familyUsersTemp = _context.tbl_User.Where(x => familyusersIds.Contains((Guid)x.UserId)).ToList();

                        foreach (var fu in familyUsers)
                        {
                            var taskForUser = _context.tbl_Task.Where(x => x.UserId == fu.UserId).Select(t => t.TaskId).ToList();
                            var taskForUserFromJoiner = _context.tbl_TaskHoursByUserJoiner.Where(x => x.UserId == fu.UserId).Select(t => t.TaskHoursId).ToList();
                            if (taskForUser.Count > 0 || taskForUserFromJoiner.Count > 0)
                            {
                                tasksHoursForUser = _context.tbl_TaskHours.Where(x => taskForUser.Contains((Guid)x.TaskId) || taskForUserFromJoiner.Contains(x.TaskHoursId)).ToList();
                                userTHToRemove = new List<tbl_TaskHours>();
                                foreach (var thfu in tasksHoursForUser)
                                {
                                    if (thfu.StartTask.Value.Date == th.StartTask.Value.Date && thfu.EndTask.Value.Date == th.EndTask.Value.Date &&
                                        ((thfu.StartTask.Value.TimeOfDay <= th.StartTask.Value.TimeOfDay && thfu.EndTask.Value.TimeOfDay >= th.EndTask.Value.TimeOfDay) ||
                                        (thfu.StartTask.Value.TimeOfDay <= th.StartTask.Value.TimeOfDay && thfu.EndTask.Value.TimeOfDay >= th.StartTask.Value.TimeOfDay) ||
                                        (thfu.StartTask.Value.TimeOfDay <= th.EndTask.Value.TimeOfDay && thfu.EndTask.Value.TimeOfDay >= th.EndTask.Value.TimeOfDay)))
                                    {
                                        userTHToRemove.Add(thfu);
                                    }
                                }
                                if (userTHToRemove.Count > 0)
                                {
                                    familyUsersTemp.Remove(fu);
                                }
                            }
                        }
                        //foreach (var remove in familyUsersTemp)
                        //{
                        //    familyUsers.Remove(remove);
                        //}
                        
                        if (familyUsersTemp.Count == 0) {
                            noMatch++;
                            //return "Users busy in this date & time - no user find match.";
                        }

                        Dictionary<Guid, Int64> userAndScores = new Dictionary<Guid, Int64>();
                        foreach (var fUser in familyUsersTemp)
                        {
                            var isLikeTasks = _context.tbl_IsUserLikedTask.Where(x => x.TaskId == task.TaskId && x.UserId == fUser.UserId).FirstOrDefault();
                            if (isLikeTasks != null)
                            {
                                if ((bool)isLikeTasks.IsLikeTask)
                                {
                                    var userScore = fUser.Score;
                                    userAndScores.Add(fUser.UserId, (long)(userScore + 20));
                                }
                                else
                                {
                                    var userScore = fUser.Score;
                                    userAndScores.Add(fUser.UserId, (long)(userScore - 20));
                                }
                            }
                            else
                            {
                                var userScore = fUser.Score == null ? task.TaskScore : task.TaskScore + fUser.Score;
                                userAndScores.Add(fUser.UserId, (long)userScore);
                            }
                        }
                        if (userAndScores.Count > 0)
                        {
                            userAndScores = userAndScores.OrderBy(x => x.Value).ToDictionary(x => x.Key, x => x.Value);
                            _context.tbl_TaskHoursByUserJoiner.Add(new tbl_TaskHoursByUserJoiner
                            {
                                TaskHoursByUserId = Guid.NewGuid(),
                                TaskHoursId = th.TaskHoursId,
                                UserId = userAndScores.FirstOrDefault().Key
                            });
                            _context.SaveChanges();
                        }
                    }
                    if(noMatch == taskHours.Count)
                    {
                        DeleteTask(taskHoursIds, taskHours, task);
                        return "No match found for tasks";
                    }
                    return "Multi";
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public bool IsUserBusy(DateTime startTask, DateTime endTask, tbl_User user)
        {
            try
            {
                using (HomeLife_FinalProjectEntities _context = new HomeLife_FinalProjectEntities())
                {
                    var tasksForUser = _context.tbl_Task.Where(x => x.UserId == user.UserId).Select(x => x.TaskId).ToList();
                    var tasksHoursForUserFromJoiner = _context.tbl_TaskHoursByUserJoiner.Where(x => x.UserId == user.UserId).Select(x=> x.TaskHoursId).ToList();
                    var tasksHoursForUser = _context.tbl_TaskHours.Where(x => tasksForUser.Contains((Guid)x.TaskId) || tasksHoursForUserFromJoiner.Contains(x.TaskHoursId)).ToList();
                    
                    foreach (var th in tasksHoursForUser)
                    {
                        if(th.StartTask <= startTask && th.EndTask >= endTask)
                        {
                            return false;
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                return true;
            }
        }
    }
}

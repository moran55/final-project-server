﻿using FinalProject.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalProjectDAL.BusinessEntities
{
    class TaskHoursManager
    {
        public List<TaskHours> FetchByTaskHours(DateTime startTask, DateTime endTask)
        {
            using (HomeLife_FinalProjectEntities _context = new HomeLife_FinalProjectEntities())
            {
                var tasksHours = _context.tbl_TaskHours.Where(x => x.StartTask == startTask && x.EndTask == endTask).ToList();
                List<TaskHours> tasks = new List<TaskHours>();
                foreach (var task in tasksHours)
                {
                    tasks.Add(new TaskHours()
                    {
                        TaskHoursId = task.TaskHoursId,
                        startTask = task.StartTask,
                        EndTask = task.EndTask
                    });
                }
                return tasks;
            }
        }
        
        //public List<TaskHours> FetchByDay(string dayOfTask)
        //{
        //    using (HomeLife_FinalProjectEntities _context = new HomeLife_FinalProjectEntities())
        //    {
        //        var tasksHours = _context.tbl_TaskHours.Where(x => x.DaysList.Contains(dayOfTask)).ToList();
        //        List<TaskHours> tasks = new List<TaskHours>();
        //        foreach (var task in tasksHours)
        //        {
        //            tasks.Add(new TaskHours()
        //            {
        //                TaskHoursId = task.TaskHoursId,
        //                startTask = task.StartTask,
        //                EndTask = task.EndTask,
        //                DaysList = task.DaysList
        //            });
        //        }
        //        return tasks;
        //    }
        //}
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using FinalProject.Common.Models;
using FinalProjectDAL.BusinessEntities;
using static FinalProject.Common.Models.Requests;

namespace FinalProject.Api.Controllers
{
    public class ShoppingItemsController : ApiController
    {
        private ShoppingListManager bl;
        public ShoppingItemsController()
        {
            bl = new ShoppingListManager();
        }

        public IHttpActionResult Post(CompleteChange data)
        {
            ClaimsIdentity claimsIdentity = User.Identity as ClaimsIdentity;
            var claims = claimsIdentity.Claims.Select(x => new { type = x.Type, value = x.Value });
            var userID = claims.FirstOrDefault(x => x.type.ToLower() == "sub");

            if (data.ShoppingItemsFalseIds != null || data.ShoppingItemsTrueIds != null)
                return Ok(bl.ChangeShoppingItemsIsComplete(data));
            return Ok(false);
        }

        [Route("api/ShoppingItems/AddNew")]
        public IHttpActionResult Post(AddNewShoppingItem data)
        {
            ClaimsIdentity claimsIdentity = User.Identity as ClaimsIdentity;
            var claims = claimsIdentity.Claims.Select(x => new { type = x.Type, value = x.Value });
            var userID = claims.FirstOrDefault(x => x.type.ToLower() == "sub");

            if (data != null)
                return Ok(bl.AddNewShoppingItem(data));
            return Ok(false);
        }

        public IHttpActionResult Delete(ShoppingItemToDelete data)
        {
            if (data.ShoppingItemId != null && data.ShoppingItemId != String.Empty)
                return Ok(bl.DeleteShoppingItem(data.ShoppingItemId));
            return Ok(false);

        }

    }
}

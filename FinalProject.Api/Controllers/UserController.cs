﻿using FinalProject.Common.Models;
using FinalProjectDAL.BusinessEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using static FinalProject.Common.Models.Requests;

namespace FinalProject.Api.Controllers
{
    public class UserController : ApiController
    {
        private UserManager bl;
        public UserController()
        {
            bl = new UserManager();
        }

        public IHttpActionResult Post(NewUser user)
        {
            var message = bl.AddNewUser(user);
            return Ok(message);
        }

        public IHttpActionResult Get()
        {
            ClaimsIdentity claimsIdentity = User.Identity as ClaimsIdentity;
            var claims = claimsIdentity.Claims.Select(x => new { type = x.Type, value = x.Value });
            var userID = claims.FirstOrDefault(x => x.type.ToLower() == "sub");

            
            var user = bl.Fetch(Guid.Parse(userID.value));
            return Ok(user);
        }

        public IHttpActionResult Put(User user)
        {
            var message = bl.UpdateUser(user);
            return Ok(message);
        }

    }
}

﻿using FinalProject.Common;
using FinalProject.Common.Models;
using FinalProjectDAL.BusinessEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Security.Claims;
using static FinalProject.Common.Models.Requests;

namespace FinalProject.Api.Controllers
{
    [Authorize]
    public class TaskController : ApiController
    {
        private TaskManager bl;
        public TaskController()
        {
            bl = new TaskManager();
        }
        
        public IHttpActionResult Post(NewTask task)
        {
            ClaimsIdentity claimsIdentity = User.Identity as ClaimsIdentity;
            var claims = claimsIdentity.Claims.Select(x => new { type = x.Type, value = x.Value });
            var userID = claims.FirstOrDefault(x => x.type.ToLower() == "sub");

            var messege = bl.AddNewTask(task, userID.value);
            return Ok(messege);
        }

        public IHttpActionResult Get()
        {
            ClaimsIdentity claimsIdentity = User.Identity as ClaimsIdentity;
            var claims = claimsIdentity.Claims.Select(x => new { type = x.Type, value = x.Value });
            var userID = claims.FirstOrDefault(x => x.type.ToLower() == "sub");

            var messege = bl.FetchTasksByUserId(Guid.Parse(userID.value));

            return Ok(messege);
        }

        public IHttpActionResult Put(UpdateTask task)
        {
            ClaimsIdentity claimsIdentity = User.Identity as ClaimsIdentity;
            var claims = claimsIdentity.Claims.Select(x => new { type = x.Type, value = x.Value });
            var userID = claims.FirstOrDefault(x => x.type.ToLower() == "sub");

            var messege = bl.UpdateTask(task, userID.value);
            return Ok(messege);
        }

        [Route("api/Task/AutomaticAlgorithmTaskByUsers")]
        public IHttpActionResult Put(AlgorithmTTaskByUser taskByUser)
        {
            ClaimsIdentity claimsIdentity = User.Identity as ClaimsIdentity;
            var claims = claimsIdentity.Claims.Select(x => new { type = x.Type, value = x.Value });
            var userID = claims.FirstOrDefault(x => x.type.ToLower() == "sub");

            var messege = "bla"; //bl.UpdateTask(task);
            return Ok(messege);
        }

        [Route("api/Task/isLikeTasks")]
        public IHttpActionResult Put(List<TasksListForIsLike> tasksIsLike)
        {
            ClaimsIdentity claimsIdentity = User.Identity as ClaimsIdentity;
            var claims = claimsIdentity.Claims.Select(x => new { type = x.Type, value = x.Value });
            var userID = claims.FirstOrDefault(x => x.type.ToLower() == "sub");

            var messege = bl.UpdateTasksIsLikeByUser(tasksIsLike, userID.value);
            return Ok(messege);
        }

        public IHttpActionResult Delete(RemoveTask taskId)
        {
            ClaimsIdentity claimsIdentity = User.Identity as ClaimsIdentity;
            var claims = claimsIdentity.Claims.Select(x => new { type = x.Type, value = x.Value });
            var userID = claims.FirstOrDefault(x => x.type.ToLower() == "sub");

            var messege = bl.DeleteTask(taskId);
            return Ok(messege);
        }
    }
}

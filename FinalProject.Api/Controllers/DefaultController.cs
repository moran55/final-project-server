﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace FinalProject.Api.Controllers
{
    public class DefaultController : ApiController
    {
        public IHttpActionResult Post()
        {
            return Ok();
        }
    }
}

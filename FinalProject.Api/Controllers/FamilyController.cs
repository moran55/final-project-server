﻿using FinalProjectDAL.BusinessEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using static FinalProject.Common.Models.Requests;

namespace FinalProject.Api.Controllers
{
    [Authorize]
    public class FamilyController : ApiController
    {
        private FamilyManager bl;
        public FamilyController()
        {
            bl = new FamilyManager();
        }

        public IHttpActionResult Post(NewMember member)
        {
            ClaimsIdentity claimsIdentity = User.Identity as ClaimsIdentity;
            var claims = claimsIdentity.Claims.Select(x => new { type = x.Type, value = x.Value });
            var userID = claims.FirstOrDefault(x => x.type.ToLower() == "sub");

            var message = bl.AddUserAsFamilyMember(member);
            return Ok(message);
        }



        public IHttpActionResult Get()
        {
            ClaimsIdentity claimsIdentity = User.Identity as ClaimsIdentity;
            var claims = claimsIdentity.Claims.Select(x => new { type = x.Type, value = x.Value });
            var userID = claims.FirstOrDefault(x => x.type.ToLower() == "sub");
            
            var FamilyUsers = bl.FetchByUserID(new Guid(userID.value));
            if (FamilyUsers != null)
                return Json(FamilyUsers);
            else
                return Ok(false);
        }

        [Route("api/Family/GetFamilyName")]
        public IHttpActionResult Post()
        {
            ClaimsIdentity claimsIdentity = User.Identity as ClaimsIdentity;
            var claims = claimsIdentity.Claims.Select(x => new { type = x.Type, value = x.Value });
            var userID = claims.FirstOrDefault(x => x.type.ToLower() == "sub");

            var familyName = bl.GetFamilyName(new Guid(userID.value));
            return Ok(familyName);
        }

        [Route("api/Family/SetFamilyName")]
        public IHttpActionResult Post(string familyName)
        {
            ClaimsIdentity claimsIdentity = User.Identity as ClaimsIdentity;
            var claims = claimsIdentity.Claims.Select(x => new { type = x.Type, value = x.Value });
            var userID = claims.FirstOrDefault(x => x.type.ToLower() == "sub");

            var newFamily = bl.setFamilyName(new Guid(userID.value), familyName);
            return Ok(newFamily);
        }
    }
}

﻿using FinalProject.Common.Models;
using FinalProjectDAL.BusinessEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;

namespace FinalProject.Api.Controllers
{
    [Authorize]
    public class LoginController : ApiController
    {
        private UserManager bl;
        public LoginController()
        {
            bl = new UserManager();
        }
        public IHttpActionResult Post()
        {
            ClaimsIdentity claimsIdentity = User.Identity as ClaimsIdentity;
            var claims = claimsIdentity.Claims.Select(x => new { type = x.Type, value = x.Value });
            var userID = claims.FirstOrDefault(x => x.type.ToLower() == "sub");

            var user = bl.Fetch(Guid.Parse(userID.value));

            //var resUser = bl.FetchLoginUser(user);
            return Ok(user);
        }

        public IHttpActionResult Get(String userName)
        {
            var messege = bl.CheckIfUserNameExist(userName);
            return Ok(messege);
        }
    }
}

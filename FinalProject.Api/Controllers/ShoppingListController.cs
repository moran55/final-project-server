﻿using FinalProject.Common.Models;
using FinalProjectDAL.BusinessEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using static FinalProject.Common.Models.Requests;

namespace FinalProject.Api.Controllers
{
    //[Authorize]
    public class ShoppingListController : ApiController
    {
        private ShoppingListManager bl;
        public ShoppingListController()
        {
            bl = new ShoppingListManager();
        }
        public IHttpActionResult Post(NewShoppingList shoppingList)
        {
            ClaimsIdentity claimsIdentity = User.Identity as ClaimsIdentity;
            var claims = claimsIdentity.Claims.Select(x => new { type = x.Type, value = x.Value });
            var userID = claims.FirstOrDefault(x => x.type.ToLower() == "sub");
            
            var message = bl.AddNewShoppingList(shoppingList,userID.value);
            return Ok(message);
        }

        public IHttpActionResult Get()
        {
            ClaimsIdentity claimsIdentity = User.Identity as ClaimsIdentity;
            var claims = claimsIdentity.Claims.Select(x => new { type = x.Type, value = x.Value });
            var userID = claims.FirstOrDefault(x => x.type.ToLower() == "sub");

            UserManager us = new UserManager();
            var user = us.Fetch(Guid.Parse(userID.value));

            if(user.FamilyId != Guid.Empty && user.FamilyId != null)
            {
                var shoppingLists = bl.FetchByFamilyId((Guid)user.FamilyId);
                if (shoppingLists != null)
                    return Json(shoppingLists);
                else
                    return Ok(false);
            }
            return Ok(false);
        }

        [Route("api/ShoppingList/GetTasks")]
        public IHttpActionResult Post()
        {
            ClaimsIdentity claimsIdentity = User.Identity as ClaimsIdentity;
            var claims = claimsIdentity.Claims.Select(x => new { type = x.Type, value = x.Value });
            var userID = claims.FirstOrDefault(x => x.type.ToLower() == "sub");

            var tasks = bl.GetTasks(Guid.Parse(userID.value));
            
            return Ok(tasks);
        }

        public IHttpActionResult Put(Guid ShoppingListId)
        {
            ClaimsIdentity claimsIdentity = User.Identity as ClaimsIdentity;
            var claims = claimsIdentity.Claims.Select(x => new { type = x.Type, value = x.Value });
            var userID = claims.FirstOrDefault(x => x.type.ToLower() == "sub");

            if (ShoppingListId != null)
                    return Ok(bl.DeleteShoppingList(ShoppingListId));
            return Ok(false);
        }
        [Route("api/ShoppingList/SaveSLToTask")]
        public IHttpActionResult Put(ShoppingToTask slAndTask)
        {
            ClaimsIdentity claimsIdentity = User.Identity as ClaimsIdentity;
            var claims = claimsIdentity.Claims.Select(x => new { type = x.Type, value = x.Value });
            var userID = claims.FirstOrDefault(x => x.type.ToLower() == "sub");

            var message = bl.SaveShoppingListToTask(slAndTask);
            return Ok(message);
        }
    }
}

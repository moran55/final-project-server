﻿using FinalProject.Common.Models;
using FinalProjectDAL.BusinessEntities;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;

namespace FinalProject.Api.Providers
{
    public class CustomOAuthProvider : OAuthAuthorizationServerProvider
    {

        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated();
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            UserManager user = new UserManager();
            var result = user.FetchLoginUser(new User()
            {
                UserName = context.UserName,
                Password = context.Password
            });
            if (result == null)
            {
                context.SetError("invalid_grant", "The User name or Password is incorrect.");
                return;
            }

            var identity = new ClaimsIdentity(context.Options.AuthenticationType);
            identity.AddClaim(new Claim("sub", result.UserId.ToString()));
            identity.AddClaim(new Claim("role", "user"));
            context.Validated(identity);
        }
    }
}